package com.pactton.Universidad;

public class Curso {

	
	int cod;
	private String nombre;
	private int cargaHoras;
	private Profesor profesor;
	
	public Curso(int cod, String nombre, int cargaHoras) {
		super();
		this.cod = cod;
		this.nombre = nombre;
		this.cargaHoras = cargaHoras;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCargaHoras() {
		return cargaHoras;
	}

	public void setCargaHoras(int cargaHoras) {
		this.cargaHoras = cargaHoras;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	

	
}

