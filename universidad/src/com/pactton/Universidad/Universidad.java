package com.pactton.Universidad;

import java.util.ArrayList;

public class Universidad {

	
	private String nombre;
	private String tipo;
	private ArrayList<Alumno> alumnos;
	
	public Universidad(String nombre, String tipo) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		
		alumnos = new ArrayList<Alumno>();
	}
	
	public void inscribirAlumno(Alumno alum) {
		
		this.alumnos.add(alum);
	}
	
	public void buscarXdni(int dni) {
		
		boolean esta=false;
		
		for(Alumno alum: this.alumnos) {
			
			if(alum.getDni()==dni) {
				
				System.out.println("el dni se encontr�: "+dni);
				esta=true;
			}
			
		if (esta=false) {
			
			System.out.println("No se encontr� el dni");
		}
			
			
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public ArrayList<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(ArrayList<Alumno> alumnos) {
		this.alumnos = alumnos;
	}
	
	
	
	
}
