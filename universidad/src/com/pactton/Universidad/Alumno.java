package com.pactton.Universidad;

import java.util.ArrayList;

public class Alumno {
	
	private int dni;
	private String nombre;
	private String apellido;
	private ArrayList<Curso> cursos;


	public Alumno(int dni, String nombre, String apellido) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.cursos = new ArrayList<Curso>();
	}

	public Alumno() {
		super();
		this.cursos = new ArrayList<Curso>();
	}

	

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void agregarCurso(Curso cur) {
		
		this.cursos.add(cur);
		
	}
	
	public void listarCursos() {
		
		System.out.println("Listado de cursos inscriptos \n");
		for (Curso cur: this.cursos ) {
			
			System.out.println("C�digo: "+cur.getCod());
			System.out.println("Nombre: "+cur.getNombre());
			System.out.println("Profesor del curso: "+cur.getProfesor().getNombre());
		}
	}
	
	

}
