package programa;

import com.pactton.Universidad.Alumno;
import com.pactton.Universidad.Curso;
import com.pactton.Universidad.Profesor;
import com.pactton.Universidad.Universidad;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Alumno a1= new Alumno(22222,"Maria","Lopez");
		Alumno a2= new Alumno(45551,"Gabriel","Perez");
		
		Curso c1= new Curso (11455, "Analisis 1", 12);
		Curso c2= new Curso (14565, "Fisica", 8);
		Curso c3= new Curso (45455, "Filosofia",10);
		
		Profesor p1= new Profesor("442464","Pf Juan", "Rossi");
		Profesor p2= new Profesor("111144","Pf Maria", "Hernandez");
		Profesor p3= new Profesor("221244","Pf Angel", "Rodriguez");
		
		
		c1.setProfesor(p1);
		c2.setProfesor(p2);
		c3.setProfesor(p3);
		
		a1.agregarCurso(c1);
		a1.agregarCurso(c2);
		a1.agregarCurso(c3);
		
		
		System.out.println(c1.getNombre()+ " tiene como profesor " + c1.getProfesor().getNombre());
		
		a1.listarCursos();
		
		Universidad uni= new Universidad("UBA","Ingenieria");
		
		uni.inscribirAlumno(a1);
		uni.inscribirAlumno(a2);
		
		System.out.println("................................................................................");
		uni.buscarXdni(45551);
		

	}

}
